# Кастомизация шаблона

```
Package name, version, version code:
  config.xml widget

Admob app id:
  config.xml widget > plugin[name="cordova-plugin-admob-free"]

Admob banner id:
  src/environments/environment-base.ts

Currency service url:
  src/environments/environment-base.ts
```

# Сборка

На компьютере должны быть установлены:
- java 1.8 (Oracle или OpenJDK)
- Android SDK
- node, npm
- ionic, cordova (`npm i -g ionic cordova`)

Установка зависимостей проекта:
```
npm i
```

Сборка отладочной версии и запуск на подключенном устройстве:
```
ionic cordova run android
```

Сборка релизной версии без подписи apk
```
ionic cordova build android --prod --release
```
