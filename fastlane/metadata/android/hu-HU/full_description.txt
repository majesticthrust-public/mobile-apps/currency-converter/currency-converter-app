Valutaváltó több mint 180 pénznemre. Valós idejű árfolyamok.

Főbb jellemzői:
- A pénznemek listáját kezelheti fontos devizái (hamarosan!)
- Több pénznem konvertálása
- Kedvenc pénznemek, amelyeket gyakran használ (hamarosan!)
- Kedvencek egyszerre konvertálhatók (hamarosan!)
- Élő árfolyamok lekérése
- Élő valutacsere
- Több mint 180 világ valuta és devizaárfolyam
- Az élő devizaárfolyamok automatikus lekérése