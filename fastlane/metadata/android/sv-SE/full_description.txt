Valutakonverterare för över 180 valutor. Realtidskurser.

Viktigaste egenskaper:
- Du kan hantera din valutarlista för dina viktiga valutor (kommer snart!)
- Konvertera flera valutor
- Favoritvaluta som du ofta använder (kommer snart!)
- Favoriter konvertera samtidigt (kommer snart!)
- Hämta live-priser
- Levande växling av valuta
- Över 180 världsvalutor och valutakurser
- Hämta live valutakurser automatiskt