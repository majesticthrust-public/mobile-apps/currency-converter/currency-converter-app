Convertidor de divisas para más de 180 divisas. Tipos de cambio en tiempo real.

Principales características:
- Puede administrar su lista de monedas para sus monedas importantes (¡próximamente!)
- Convertir varias monedas
- Las monedas favoritas que usa con frecuencia (¡próximamente!)
- Conversión de favoritos al mismo tiempo (¡próximamente!)
- Obtener tarifas en vivo
- Cambio de moneda en vivo.
- Más de 180 monedas mundiales y tipos de cambio.
- Obtener automáticamente las tasas de cambio en vivo