Valutaomregner for over 180 valutaer. Real-time valutakurser.

Hovedtræk:
- Du kan administrere din valuta liste for dine vigtige valutaer (kommer snart!)
- Konverter flere valutaer
- Favoritvalutaer, som du ofte bruger (kommer snart!)
- Favoritter konverteres på samme tid (kommer snart!)
- Hent live satser
- Levende valutaudveksling
- Over 180 verdensvalutaer og valutakurser
- Hent automatisk live valutakurser