Konvertor meny pre viac ako 180 mien. Výmenný kurz v reálnom čase.

Hlavné rysy:
- Môžete spravovať svoj zoznam mien pre vaše dôležité meny (čoskoro!)
- Prevod viacerých mien
- obľúbené meny, ktoré často používate (čoskoro!)
- Obľúbené konverziu v rovnakom čase (čoskoro!)
- Vyzdvihnúť sadzby za živý
- Živá výmena meny
- viac ako 180 svetových mien a kurzov cudzích mien
- Automatické načítanie výmenných kurzov