Valiutos konverteris daugiau nei 180 valiutų. Valiutų kursai realiuoju laiku.

Pagrindinės funkcijos:
- Galite tvarkyti savo valiutų sąrašą savo svarbioms valiutoms (netrukus!)
- Konvertuoti kelias valiutas
- Mėgstamiausios valiutos, kurias dažnai naudojate (netrukus!)
- Parankiniai konvertuojami tuo pačiu metu (netrukus!)
- pareikšti tiesioginius tarifus
- Tiesioginis valiutos keitimas
- Daugiau nei 180 pasaulio valiutų ir užsienio valiutų kursų
- Automatiškai atsiimti gyvus užsienio valiutos kursus