180-dən çox valyuta üçün valyuta çeviricisi. Real vaxt valyuta məzənnələri.

Əsas xüsusiyyətləri:
- Valyuta siyahınızı əhəmiyyətli valyutalarınız üçün idarə edə bilərsiniz (tezliklə!)
- Bir çox valyutanı çevirmək
- Tez-tez istifadə etdiyiniz sevimli valyutalar (tezliklə!)
- Favoriler eyni zamanda (tezliklə) çevirirlər!
- Canlı qiymətləri əldə edin
- Valyuta mübadiləsi
- 180-dən çox dünya valyutası və valyuta məzənnələri
- Avtomatik olaraq xarici valyuta məzənnələrini əldə edin