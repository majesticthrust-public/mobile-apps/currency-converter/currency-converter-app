fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android build_debug
```
fastlane android build_debug
```
Build Debug
### android build_release
```
fastlane android build_release
```
Build Release
### android upload_apk
```
fastlane android upload_apk
```
Upload apk
### android update_store_text
```
fastlane android update_store_text
```
Update Store Text
### android update_store_images
```
fastlane android update_store_images
```
Update Store Images
### android update_store_screenshots
```
fastlane android update_store_screenshots
```
Update Store Screenshots

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
