export default {
  admob: {
    isEnabled: true,

    /**
     * ID баннера адмоб
     */
    bannerId: 'YOUR_ADMOB_BANNER_ID'
  },
  currency_service: {
    /**
     * Урл сервиса, где кешируются обменные курсы;
     * Например https://www.example.com.
     */
    host_name: 'YOUR_CURRENCY_SERVICE_URL'
  }
};
