import _ from 'lodash';
import baseEnvironment from './environment-base';


export const environment = _.merge({
  production: true,
  admob: {
    isEnabled: true
  }
}, baseEnvironment);
