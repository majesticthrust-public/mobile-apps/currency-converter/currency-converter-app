import { Component, ViewChild } from '@angular/core';
import { CurrencyService, Currency } from '../services/currencies/currency.service';
import { CurrencySelectComponent } from '../components/currency-select/currency-select.component';
import { CalculatorValue } from '../components/calculator/calculator.component';

@Component({
  selector: 'app-pairwise-conversion',
  templateUrl: 'pairwise-conversion.page.html',
  styleUrls: ['pairwise-conversion.page.scss'],
})
export class PairwiseConversionPage {
  @ViewChild('selectConvertFrom', { static: true }) currencyFromComponent: CurrencySelectComponent;
  @ViewChild('selectConvertTo', { static: true }) currencyToComponent: CurrencySelectComponent;

  currencyFrom: Currency;
  currencyTo: Currency;

  // не очень красиво получается, но да пох
  calculatorValue: CalculatorValue = { string: '0', number: 0 };
  _currencyToValue = 0;
  get currencyToValue() {
    let v = this._currencyToValue;
    // округление до 2 знаков после запятой
    v = Math.round(v * 100) / 100;
    return v;
  }
  set currencyToValue(v) {
    this._currencyToValue = v;
  }

  constructor(private currencyService: CurrencyService) {}

  selectCurrencyFromClick() {
    this.currencyFromComponent.open();
  }

  selectCurrencyToClick() {
    this.currencyToComponent.open();
  }

  setCurrencyFrom(currencyAbbr: string) {
    console.log('setCurrencyFrom', currencyAbbr);
    this.currencyFrom = this.currencyService.getCurrency(currencyAbbr);
    console.log(this.currencyFrom);
  }

  setCurrencyTo(currencyAbbr: string) {
    console.log('setCurrencyTo', currencyAbbr);
    this.currencyTo = this.currencyService.getCurrency(currencyAbbr);
    console.log(this.currencyTo);
  }

  swapCurrencies() {
    // кешируем, потому что при изменении сразу поднимается соответствующее событие
    // и на этой странице сразу обновляется поле currencyFrom
    const currencyToAbbr = this.currencyTo.abbreviation;
    const currencyFromAbbr = this.currencyFrom.abbreviation;

    this.currencyFromComponent.setCurrency(currencyToAbbr);
    this.currencyToComponent.setCurrency(currencyFromAbbr);

    this.recalculateCurrencyToValue();
  }

  calculatorInput(value: CalculatorValue) {
    this.calculatorValue = value;
    this.recalculateCurrencyToValue();
  }

  recalculateCurrencyToValue() {
    this.currencyToValue = this.currencyService.convert(
      this.currencyFrom.abbreviation,
      this.currencyTo.abbreviation,
      this.calculatorValue.number);
  }
}
