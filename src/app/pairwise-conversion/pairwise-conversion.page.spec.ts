import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PairwiseConversionPage } from './pairwise-conversion.page';

describe('PairwiseConversionPage', () => {
  let component: PairwiseConversionPage;
  let fixture: ComponentFixture<PairwiseConversionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PairwiseConversionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PairwiseConversionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
