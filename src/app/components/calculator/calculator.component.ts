import { Component, Output, EventEmitter } from '@angular/core';

export interface CalculatorValue {
  number: number;
  string: string;
}

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent {
  _inputSymbols = '';
  get inputSymbols() {
    return this._inputSymbols;
  }
  set inputSymbols(val) {
    this._inputSymbols = val;
    this.valueChange.emit(this.value);
  }

  public get value(): CalculatorValue {
    const val = parseFloat(this.inputSymbols);
    return {
      number: isNaN(val) ? 0 : val,
      string: this.inputSymbols.length === 0 ? '0' : this.inputSymbols
    };
  }

  @Output() valueChange = new EventEmitter<CalculatorValue>();
  @Output() swap = new EventEmitter<void>();

  constructor() { }

  enterSymbol(symbol: ('1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'|'0'|'.')) {
    // can't put multiple .
    if (symbol === '.' && this.inputSymbols.includes('.')) {
      return;
    }

    this.inputSymbols += symbol;
  }

  deleteSymbol() {
    if (this.inputSymbols.length > 0) {
      this.inputSymbols = this.inputSymbols.slice(0, -1);
    }
  }

  clearSymbols() {
    this.inputSymbols = '';
  }

  swapCurrencies() {
    this.swap.emit();
  }
}
