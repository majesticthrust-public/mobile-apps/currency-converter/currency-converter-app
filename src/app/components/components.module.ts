import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { CurrencySelectComponent } from './currency-select/currency-select.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { CurrencySelectButtonComponent } from './currency-select-button/currency-select-button.component';
import { SquareDivComponent } from './square-div/square-div.component';

@NgModule({
  declarations: [CurrencySelectComponent, CalculatorComponent, CurrencySelectButtonComponent, SquareDivComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicSelectableModule
  ],
  exports: [
    CurrencySelectComponent,
    CalculatorComponent,
    CurrencySelectButtonComponent
  ]
})
export class ComponentsModule { }
