import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-square-div',
  templateUrl: './square-div.component.html',
  styleUrls: ['./square-div.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SquareDivComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
