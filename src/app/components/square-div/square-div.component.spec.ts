import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquareDivComponent } from './square-div.component';

describe('SquareDivComponent', () => {
  let component: SquareDivComponent;
  let fixture: ComponentFixture<SquareDivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquareDivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquareDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
