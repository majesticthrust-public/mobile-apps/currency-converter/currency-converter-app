import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-currency-select-button',
  templateUrl: './currency-select-button.component.html',
  styleUrls: ['./currency-select-button.component.scss']
})
export class CurrencySelectButtonComponent implements OnInit {
  @Input() imgSrc: string;

  constructor() { }

  ngOnInit() {}
}
