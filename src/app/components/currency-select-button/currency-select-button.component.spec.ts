import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencySelectButtonComponent } from './currency-select-button.component';

describe('CurrencySelectButtonComponent', () => {
  let component: CurrencySelectButtonComponent;
  let fixture: ComponentFixture<CurrencySelectButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencySelectButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencySelectButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
