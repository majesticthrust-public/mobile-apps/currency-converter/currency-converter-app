import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import {
  CurrencyService,
  Currency as CurrencyData
} from 'src/app/services/currencies/currency.service';
import { IonicSelectableComponent } from 'ionic-selectable';

// caching normalized fields for searching
interface CurrencySearchable {
  _abbreviation: string;
  _name: string;
}

type Currency = CurrencyData & CurrencySearchable;

@Component({
  selector: 'app-currency-select',
  templateUrl: './currency-select.component.html',
  styleUrls: ['./currency-select.component.scss']
})
export class CurrencySelectComponent implements OnInit {
  currencies: Currency[];
  currency: Currency;

  /**
   * Popover list title
   */
  @Input() listTitle = 'Select';

  /**
   * Initial selected value. Currency abbreviation, case-incensitive.
   */
  @Input() initialValue: string;

  /**
   * Currently selected value.
   */
  @Output() selectedValue = new EventEmitter<string>();

  @ViewChild('selectableComponent', { static: true })
  selectableComponent: IonicSelectableComponent;

  constructor(private currencyService: CurrencyService) {
    // normalize and cache searched fields
    const currencyData = this.currencyService.getCurrencies();
    this.currencies = currencyData.map(currency => {
      const searchable: CurrencySearchable = {
        _abbreviation: currency.abbreviation.toLowerCase(),
        _name: currency.name.toLowerCase()
      };

      return Object.assign(searchable, currency);
    });
  }

  ngOnInit() {
    // set initival value
    if (this.initialValue) {
      const init = this.initialValue.toLowerCase();

      const foundCurrency = this.currencies.find(
        currency => currency._abbreviation === init
      );
      if (foundCurrency) {
        // emit initial selection
        this.selectedValue.emit(this.initialValue);

        // set currency as selected
        this.currency = foundCurrency;
      } else {
        console.warn(
          `Setting initial value for currency select failed! Currency '${this.initialValue}' not found.`
        );
      }
    }
  }

  currencyChange(event: {
    component: IonicSelectableComponent;
    value: Currency;
  }) {
    this.selectedValue.emit(event.value.abbreviation);
  }

  filterCurrencies(currencies: Currency[], text: string) {
    text = text.trim().toLowerCase();

    return currencies.filter(currency => {
      return (
        currency._name.indexOf(text) !== -1 ||
        currency._abbreviation.indexOf(text) !== -1 ||
        currency.symbol.indexOf(text) !== -1
      );
    });
  }

  currencySearch(event: { component: IonicSelectableComponent; text: string }) {
    const { text } = event;
    event.component.startSearch();

    if (text) {
      event.component.items = this.filterCurrencies(this.currencies, text);
    } else {
      event.component.items = this.currencies;
    }

    event.component.endSearch();
  }

  /**
   * Open select dialog programmatically
   */
  public open() {
    return this.selectableComponent.open();
  }

  /**
   * Select currency programmatically
   * @param abbr currency abbreviation
   */
  public setCurrency(abbr) {
    const currency = this.currencies.find(c => c.abbreviation === abbr);
    if (currency) {
      this.currency = currency;
      this.selectedValue.emit(abbr);
    }
  }
}
