import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'pairwise-conversion', pathMatch: 'full' },
  { path: 'pairwise-conversion', loadChildren: './pairwise-conversion/pairwise-conversion.module#PairwiseConversionPageModule' },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
