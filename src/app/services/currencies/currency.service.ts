import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import getSymbolFromCurrency from 'currency-symbol-map';
import currenciesJson from './currencies.json';
import { environment } from '../../../environments/environment';


export interface Currency {
  abbreviation: string;
  name: string;
  imagePath: string;
  symbol: string;
}

interface CurrencyRates {
  [abbr: string]: number;
}

interface FixerLatestResponse {
  success: boolean;
  timestamp: number;
  base: string;
  date: string;
  rates: CurrencyRates;
}

// TODO кеширование курсов и автоматическое обновление с заданной частотой
// TODO любимые валюты, сохранение в storage

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  private abbreviations: string[];
  private currencies: Currency[];

  private rates: CurrencyRates;

  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.currency_service.host_name;

    this.abbreviations = Object.keys(currenciesJson);

    this.currencies = this.abbreviations.map(abbr => {
      const { name, imagePath } = currenciesJson[abbr];

      return {
        abbreviation: abbr,
        name,
        imagePath: imagePath || 'assets/Solid_white.svg',
        symbol: getSymbolFromCurrency(abbr) || ''
      };
    });

    const latestUrl = this.baseUrl + '/fixer/latest';
    this.http.get<FixerLatestResponse>(latestUrl).toPromise()
      .then(response => {
        console.log('Currency api response', response);
        this.rates = response.rates;
      })
      .catch(e => console.error('Error getting currency rates', e));
  }

  /**
   * Возвращает список всех доступных валют
   */
  public getCurrencies() {
    return this.currencies;
  }

  /**
   * Возвращает валюту по аббревиатуре
   * @param abbr аббревиатура
   */
  public getCurrency(abbr: string) {
    return this.currencies.find(currency => currency.abbreviation === abbr);
  }

  /**
   * Перевести сумму из одной валюты в другую
   * @param from аббревиатура валюты
   * @param to аббревиатура валюты
   * @param amount денежная сумма
   */
  public convert(from: string, to: string, amount: number) {
    const fromRate = this.rates[from];
    const toRate = this.rates[to];

    return amount / fromRate * toRate;
  }
}
