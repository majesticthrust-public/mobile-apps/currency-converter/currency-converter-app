import { Injectable } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdmobService {

  constructor(private admob: AdMobFree) { }

  getBannerConfig(): AdMobFreeBannerConfig {
    return {
      autoShow: true,
      id: environment.admob.bannerId,
      isTesting: !environment.production
    };
  }
}
