import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AdmobService } from './services/admob.service';
import { AdMobFree } from '@ionic-native/admob-free/ngx';
import { AppRate } from '@ionic-native/app-rate/ngx';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private admobService: AdmobService,
    private admob: AdMobFree,
    private appRate: AppRate
  ) {
    this.initializeApp();
    console.log('Current environment', environment);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      this.splashScreen.hide();

      // admob banner
      if (environment.admob.isEnabled) {
        console.log('Admob enabled');
        const bannerConfig = this.admobService.getBannerConfig();
        this.admob.banner.config(bannerConfig);
        this.admob.banner.prepare();
      } else {
        console.log('Admob disabled');
      }

      // app rate
      this.appRate.preferences.storeAppURL = {
        android: 'market://details?id=com.sisosysa.CurrencyConverterApp'
      };
      this.appRate.preferences.callbacks = {
        handleNegativeFeedback: () => {
          window.open('mailto:vasisyso7@gmail.com', '_system');
        }
      };
      this.appRate.promptForRating(false);
    });
  }
}
